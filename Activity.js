// 2

db.fruits.aggregate([

    { $match: { onSale: true} },
    { $group: { _id: "$supplier_ID", total: {$sum: "$stock"}} }

]);

// 3

db.fruits.aggregate([

    { $match: { stock: {$gt: 20 }} },
    { $group: { _id: "$name", total: {$sum: "$stock"}} }

]);


// 4

db.fruits.aggregate([

    { $match: { onSale: true} },
    { $group: { _id: "$supplier_ID", average: {$avg: "$stock"}} }

]);

// 5
db.fruits.aggregate([

    { $group: { _id: "$supplier_ID", max: {$max: "$price"}} }

]);

// 6

db.fruits.aggregate([

    { $group: { _id: "$supplier_ID", min: {$min: "$price"}} }

]);